function [evm,f,meuDelay] = calculaEVM(i1,i2,time,fLo,fHi,Nc,b)
% Descri��o:
% fun��o que calcula o EVM entre dois sinais de acordo com a banda de
% frequencias. 
% Entradas:
% i1, i2 = Sinais para calculo do EVM entre eles
% time = vetor do tempo
% fLo = frequencia inferior da banda de frequ�ncias
% fHi = frequencia superior da bada
% Nc = numero de subcarriers a ser analisado
% b = fator de rollof do filtro cosseno levantado de cada subcarrier
% Saidas:
% evm = vetor do evm em funcao do numero de subcarrier
% f = vetor das subcarriers
% meuDelay = vetor de delay encontrado durante o alinhamento (sincronismo)

% Retira os n�veis dc do sinais
x1 = i1 - mean(i1);
x2 = i2 - mean(i2);
L = length(x1);
%limita o vetor do tempo para o comprimento de i1;
t = time(1:L);
% Ts = time(3) - time(2);
%  Fs = 1/Ts;
x2 = x2(1:L);
%cria o vetor de frequencias igualmente espa�adas de acordo com Nc
f = linspace(fLo,fHi,Nc);
% largura de banda do filtro seletivo cosseno levantado
BW = (fHi-fLo)/Nc;
%armazena memoria para o evm
evm = zeros(Nc,1);
meuDelay = zeros(Nc,1);
%Loop que calcula o EVM para cada subcarrier
    for i = 1:Nc
        %Fun��es de filtragem do filtro seletivo
        % usar a filtroBW3!
%         xf = filtroBW(t,b,BW,f(i),Nfft);
%         xf = filtroBWFull(t,b,BW,f(i));   

        %fun��o que gera o filtro cosseno levantado no dominio do tempo
        rcos =  filtroBW3(time,b,BW,f(i));
        % fft + normaliza��o do filtro
        xf = fft(rcos);
        xf = xf/max(abs(xf));
        %Filtragem dos sinais no dominio da frequencia (offline)
        yf1 = fft(x1).*xf;
        yf2 = fft(x2).*xf;
        % sinais resultantes filtrados (uma subcarrier de i1 e i2)
        b1 = ifft(yf1);
        b2 = ifft(yf2);
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ALGORITMOS DE ALINHAMENTO
% Descri��o: O alinhamento � fundamental para o EVM.
% Tive que criar uma fun��o de sincronia pr�pria.
% Motivos abaixo.

        %meu algoritmo de alinhamento
        meuDelay(i) = meuAlign(b1,b2);
        %alinhando o segundo sinal de acordo com o delay
        h1 = b1;
        h2 = circshift(b2,meuDelay(i));
        
% ALGORITMO DE ALINHAMENTO USANDO FUN��O NATIVA ALIGNSIGNALS
% %%%%%%%%%%% OLD - DEPRECATED - N�O USAR %%%%%%%%%%%%%%%%%%
% DESCRI��O: A fun��o nativa, em algumas portadoras, 
% mudava o delay de alinhamento para uma rota��o de 180 graus,
% ocasionando um EVM de 200%. Em resumo, a fun��o n�o estava
% alinhando todas as portadoras de acordo com o esperado.

          %Artif�cio do Lucas 
%         Ts = t(2)-t(1);
%         amostras_por_periodo = ceil(1/f(i)/Ts);
%         maxlag(i)=floor(amostras_por_periodo/4);
%         
%         if i > 1
%             [z1,z2,delay(i)] =  alignsignals(b1,b2,maxlag(i-1)+3);
%         else
%             [z1,z2,delay(i)] =  alignsignals(b1,b2,maxlag(i));
%         end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Normaliza para o rms
r1 = rms(h1);
r2 = rms(h2);
c1 = h1/r1;
c2 = h2/r2;
%obtem o evm (rms do erro)
evm(i) = rms(c1-c2);
%Descomentar caso queira inspecionar visualmente cada portadora
%         figure;
%         plot(t,c1,'b-*');
%         hold on
%         plot(t,c2,'r-O');
%         hold on;
%         plot(t,c1-c2,'k-x');
%         legend(num2str(f(i)));   
%        
    end
end
function [mag,ph,f,y] = minhaFFT(x,Fs,nfft)
%Descri��o: fun��o que calcula e retorna magnitude e fase 
% de uma fft ajustadas para os valores de frequencia
%Entradas:
% x = vetor de amostras do sinal
% Fs = freq. de amostragem
% nfft = tamanho da fft
% Saidas:
% mag = magnitude da fft (linear)
% ph = fase da fft (em radianos)
% f = vetor das frequencias da FFT

n = 2^nextpow2(nfft); %coloca em potencia de 2 
y = fft(x,n);
f = Fs*(0:(n/2))/n;
mag = abs(y/n);
mag = mag(1:n/2+1);
ph = angle(y); 
ph = ph(1:n/2+1);

end
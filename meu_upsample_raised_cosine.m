function  x=meu_upsample_raised_cosine(vetor,n,nSymbols)
% Reamostra repedindo 'n' vezes cada amostra do vetor de entrada
%    x=meu_upsample_raised_cosine(vetor,n,numsimbolos)
% n � numero de vezes de upsample
%    x=meu_upsample_raised_cosine(vetor,n);

% Example:
%     a=rand(1,1e4);figure(1);hold off;n=10;
%     plot((1:length(a))-1,a,'b-o');hold on;
%     plot((0:(length(a)*n-1))./n,meu_upsample_raised_cosine(a,n),'m.-')
%     xlim([100 120])
%     figure(2);hold off;
%     nSymbols=1e3;
%     [f,mod]=meu_fft(meu_upsample_raised_cosine(a,n,nSymbols),n);
%     loglog(f,mod,'m');hold on
%     [f,mod]=meu_fft(a,1);
%     loglog(f,mod,'b');

if nargin <3
    nSymbols=1e2;
end
% numsimbolos � a extens�o em simbolos do pulso raised cosine
if size(vetor,1)>size(vetor,2)
    vetor=vetor';
    transposto=1;
else
    transposto=0;
end

if rem(n,1)==0 % multiplo inteiro
    upsampled_vector=upsample(vetor,n)*n;
    % centered pulses
%     upsampled_vector=[zeros(1,n/2) upsampled_vector(1:end-n/2)];
%     upsampled_vector=[zeros(1,n/2) upsampled_vector(1:end-n/2)];
    x=meu_filtro_rised_cossine(upsampled_vector,n,nSymbols);
else
    % vou procurar menor multiplo inteiro e sobreamostrar vetor, depois
    % subamostrar novamente...
    MaxOversampleTry=20;
    kGambiarra=n*(1:MaxOversampleTry);
    % menor multiplo inteiro
    kGambiarra=find(rem(kGambiarra,1)<1e-6,1,'first');
    nNew=round(kGambiarra*n);
    upsampled_vector=upsample(vetor,nNew)*nNew;
    x=meu_filtro_rised_cossine(upsampled_vector,nNew,1e3);
    % subamostrar novamente...
    x=downsample(x,kGambiarra);
%     keyboard;
%     error('''n'' deve ser inteiro!')
end
if transposto==1
    x=x';
end
    
end
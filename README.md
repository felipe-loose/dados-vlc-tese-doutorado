# Dados-VLC-Tese-Doutorado

Acesso público ao banco de dados utilizados na tese de doutorado de Felipe Loose, Santa Maria, RS, Brasil.

Public access of data bank utilized in the doctoral thesis of Felipe Loose, Santa Maria, RS, Brazil.

#### Main Matlab Files:

EVMCalculation.m is the main file

PlotDoNovoEVM.m plots the EVM Data.

Feel Free to utilize any of the .mat data files.

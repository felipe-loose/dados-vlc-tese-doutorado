function [delay] = meuAlign(x1,x2)

[c,lag] = xcorr(x1,x2);
c = c/max(c);
[m,i] = max(c);
delay = lag(i);


end

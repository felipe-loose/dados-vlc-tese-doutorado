function [Psx] = magFFT2(xf,n)
%Descri��o: Fun��o que ajeita o espectro da FFT para ser plotado em dB
% ENTRADAS 
% xf = eh a fft
% n = eh  num da fft/
Px = 20*log10(abs(xf/n));
Psx = Px(1:n/2+1);
end
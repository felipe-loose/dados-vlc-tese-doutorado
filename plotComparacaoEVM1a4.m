clc; close all; clear all;

load ComparacaoEVM1a4ComESemPE.mat
fc1 = 1e6;
fc2 = 4e6;
f = linspace(fc1,fc2,32);

% new EVM

plot(f,evmPE*100,'bO',f,evmsPE*100,'rs');
title('comparašao EVMs');
xlabel('f (Hz)');
ylabel('EVM (%)');
legend('com PE','sem PE');
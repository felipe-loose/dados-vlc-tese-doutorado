function [Psx] = magFFT(xf)
%Descri��o: Fun��o que ajeita o espectro da FFT para ser plotado em dB


L = length(xf);
Px = 20*log10(abs(xf));
Psx = Px(1:L/2+1);
Psx(2:end-1) = 2*Psx(2:end-1);

end
clc; close all; clear all;
format shorteng

%Dados retirados da simula��o SPICE
load dadosSpice.mat
iLED = iLED - mean(iLED);
vLin = vLin - mean(vLin);
vRef = vRef - mean(vRef);

Ts = time(3) - time(2);
Fs = 1/Ts;
n = 2^nextpow2(length(time));

f = Fs*(0:(n/2))/n;

Y = fft(iLED,n);
P = abs(Y/n);

semilogx(f,P(1:n/2+1)) 
title('FFT of signal')
xlabel('Frequency (f)')
ylabel('|P(f)|')
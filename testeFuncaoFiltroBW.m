clc; close all; clear all;
format shorteng
%%% COMO OPERAR ESTE SCRIPT %%%%%
% Descomentar a entrada de dados desejada logo abaixo
% a) mudar as variaveis fc1 e fc2 que definem o filtro da banda para VLC;
% b) mudar as variaveis d1 e d2, que sao usadas para obter o EVM entre
% os sinais medidos via osciloscopio;
% c) os demais plots do evm s�o dados em fun��o do que foi escolhido
% para d1 e d2;
% d) usar os gr�ficos de espectro e forma de onda para inspe��o visual
% do correto alinhament do filtro cosseno levantado ao sinal


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ENTRADA DE DADOS MEDIDOS PELO OSCILOSC�PIO

% %Dados retirados da simula��o SPICE
% n�o ta funcionando. MUITOS PONTOS
% load dadosSpice.mat
% iLED = iLED - mean(iLED);
% vLin = vLin - mean(vLin);
% vRef = vRef - mean(vRef);
% vRx = zeros(length(iLED),1);

%descomentar estes se for usar os dados antigos.
% %%%%%%%%% VALIDADO - nao tem Rx %%%%%%%%%%%%
% load dados_ILEDs_vm10.mat 
% vRef = VI10 - mean(VI10);
% iLED = ILED10 - mean(ILED10);
% iLED = iLED/rms(iLED);
% vRef = vRef/rms(vRef);
% vRx = zeros(length(iLED),1);
% vm = VM10 - mean(VM10);
% time = t;


%descomentar estes se for usar os dados medidos sem
% pre-enfase no sinal apos largura de banda do LED (1,75 MHz)

% load dadosSemPE1a4.mat
% iLED = iLED - mean(iLED);
% vRx = vRx - mean(vRx);
% vRef = vRef - mean(vRef);
% iLED = iLED/rms(iLED);
% vRx = vRx/rms(vRx);
% vRef = vRef/rms(vRef);

% Dados envolvendo somente captura de ruido
% nao funcionou.......

% load dadosRuido30cm.mat
% iLED = iLED - mean(iLED);
% vRx = vRx - mean(vRx);
% vRef = vRef - mean(vRef);
% iLED = iLED/rms(iLED);
% vRx = vRx/rms(vRx);
% vRef = vRef/rms(vRef);

% Dados do sinal com pre-enfase ap�s corte do LED

% load dados_com_PE1a4.mat
% iLED = iLED - mean(iLED);
% vRx = vRx - mean(vRx);
% vRef = vRef - mean(vRef);
% iLED = iLED/rms(iLED);
% vRx = vRx/rms(vRx);
% vRef = vRef/rms(vRef);

% NAO SEI O QUE EU FAZIA COM ISSO
% algo a ver com o filtro sem p.e.
% figure;
% semilogx(ff1,20*log10(mf1/max(mf1)));
% vRefOr = vRefOr - mean(vRefOr);
% vRefOr = vRefOr/rms(vRefOr);


% dados sem a pre enfase e limitado na bw do LED
load dados_lim_sem_pe.mat
iLED = iLED - mean(iLED);
vRx = vRx - mean(vRx);
vRef = vRef - mean(vRef);
iLED = iLED/rms(iLED);
vRx = vRx/rms(vRx);
vRef = vRef/rms(vRef);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Obt�m freq. de amostragem inicial
Ts = time(3) - time(2);
Fs = 1/Ts;
OldFs = 1/(time(3)-time(2));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Interpola para se obter maior resolucao
% motivo: baixos pontos capturados pelo osciloscopio
% Nfft = length(time);
Ninterp = 25;
newTime = time(1):Ts/Ninterp:time(end);
% newFs = Ninterp/Ts;
iLED = interp1(time,iLED,newTime,'pchip');
vRef = interp1(time,vRef,newTime,'pchip');
%usar esse abaixo se for usar dados com pe
% vRef = interp1(time,vRefOr,newTime,'pchip');
vRx = interp1(time,vRx,newTime,'pchip');

%Rearranja as variaveis para seguir com o algoritmo
time = newTime;
newTs = time(3) - time(2);
newFs = 1/newTs;
Fs = newFs;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Cria uma refer�ncia para o sinal sem P.E.
% sinal vRef2 estabelece esta referecia
% EVM soh funciona se for sinal com P.E.
% %Cria�ao de filtro P.B em 1.75 MHz
wo = 2*pi*1.85e6;
hs = tf(wo,[1 wo]);
% bode(hs);
vRef2 = lsim(hs,vRef,time);
vRef2 = vRef2';
vRef2 = vRef2/rms(vRef2);
% [magf,phf,ff] = minhaFFT(vRef,Fs,length(vRef));
[magf2,phf2,ff2] = minhaFFT(vRef2,Fs,length(vRef2));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Cria��o do filtro na bw da VLC
%roll off global do filtro cosseno levantado
b = 0.05;
%Determina os intervalos do filtro cosseno levantado
fc1 = 200e3;
fc2 = 1.75e6;
%Determina os limites de plotagem dos gr�ficos
xlim1 = 10e3;
xlim2 = 5e6;
ylim1 = -60;
ylim2 = 0;


%Cria o filtro cosseno levantado no dominio do tempo
rcos = filtroBW3(time,b,(fc2-fc1),(fc1+(fc2-fc1)/2));
%FFT do filtro
[mf1,phf1,ff1,xf] = minhaFFT(rcos,Fs,length(time));
% figure;
% semilogx(ff1,20*log10(mf1/max(mf1)));
% title('filtro cosseno levantado');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calcula a FFT
% Usado para descobrir a faixa de frequencias da banda de VLC;
[mag1,ph1,f1] = minhaFFT(iLED,Fs,length(iLED));
[mag2,ph2,f2] = minhaFFT(vRef,Fs,length(vRef));
[mag3,ph3,f3] = minhaFFT(vRx,Fs,length(vRx));
%Plot da magnitude
figure;
subplot 311
a1 = semilogx(f1,20*log10(mag1/max(mag1)),f1,20*log10(mf1/max(mf1)),'r');
xlim([xlim1 xlim2]);
ylim([ylim1 ylim2]);
title('iLED');
ylabel('dB normalized');
xlabel('f (Hz)');
legend('iLED','VLC bandwidth');
subplot 312
a2 = semilogx(f1,20*log10(mag2/max(mag2)),'r');
xlim([xlim1 xlim2]);
ylim([ylim1 ylim2]);
title('vRef');
subplot 313
a3 = semilogx(f1,20*log10(mag3/max(mag3)),'m');
xlim([xlim1 xlim2]);
ylim([ylim1 ylim2]);
title('vRx');

%%%%%%%%%% PLOT USADO PARA COMPARAR SINAIS COM P.E. E SEM P.E.
% DESCRI��O: A P.E. � RETIRADA POR MEIO DE FILTRO P.B. EM 1.75 MHz
%Plot da magnitude do sinal vRef sem P.E. e com P.E.
figure;
subplot 311
aa1 = semilogx(f1,20*log10(mag2/max(mag2)));
xlim([xlim1 xlim2]);
ylim([ylim1 ylim2]);
title('vRef com PE');
ylabel('dB normalized');
xlabel('f (Hz)');
subplot 312
aa2 = semilogx(f1,20*log10(magf2/max(magf2)),'r');
xlim([xlim1 xlim2]);
ylim([ylim1 ylim2]);
title('vRef sem P.E.');
ylabel('dB normalized');
xlabel('f (Hz)');
subplot 313
aa3 = semilogx(f1,20*log10(mag3/max(mag3)),'m');
xlim([xlim1 xlim2]);
ylim([ylim1 ylim2]);
title('vRx com P.E');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Plot dos sinais desalinhados (e nao sincronizados)
figure;
plot(time,iLED,'b-x',time,vRef,'r-o',time,vRx,'m-s');
legend('iLED','vRef','vRx');
title('sinais desalinhados');
%Plot da fase
% figure;
% xlim1 = 10e3;
% xlim2 = 5e6;
% ylim1 = -180;
% ylim2 = 180;
% subplot 311
% a1 = semilogx(f1,ph1*180/pi);
% xlim([xlim1 xlim2]);
% ylim([ylim1 ylim2]);
% title('iLED');
% ylabel('Phase (degrees)');
% xlabel('f (Hz)');
% subplot 312
% a2 = semilogx(f1,ph2*180/pi,'r');
% xlim([xlim1 xlim2]);
% ylim([ylim1 ylim2]);
% title('vRef');
% subplot 313
% a3 = semilogx(f1,ph3*180/pi,'m');
% xlim([xlim1 xlim2]);
% ylim([ylim1 ylim2]);
% title('vRx');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% sincronizacao para calcular o EVM
%%%%%%%%%%%%%%%
% Variaveis dummy para calculo do evm
%%%%%%%%%%%%%%%
d1 = vRef;
d2 = iLED;

% [iLED,vRef] =  alignsignals(d1,d2,50000,'truncate');

% Verificando visualmente se os ditos est�o sincronizados....
% figure;
% plot(time,d1/rms(d1),'b-x',time,d2/rms(d2),'m-s');
% legend('d1','d2');
% title('sinais alinhados');

%%%%%%%%%%%%%%%%%%%%%%%
%INSERIR AQUI PARAMETROS DO EVM
%%%%%%%%%%%%
%Numero de portadoras observados 
% OBS: para os dados o padrao � 32
Nc = 32;

%% Calculo do EVM
% [z1,z2] = alignsignals(vRef,iLED,[],'truncate');
[evm,fc,delay]= calculaEVM(d1,d2,time,fc1,fc2,Nc,b);

%plots do delay calculado para obter sincronismo por portadora
figure
plot(fc,delay,'bs');
legend('meu delay');

% %%%%% EVM aplicando uma media movel %%%%%%
%numero de pts para media movel
Nm = 5;
figure;
plot(fc,evm*100,'k-O');
xlabel('Frequency  (Hz)');
ylabel('EVM (%)');
hold on;
plot(fc,movmean(evm,Nm)*100,'b-x');
title('comparacao entre dados x media movel');
legend('dados','media movel');
figure;
bar(fc,evm*100);
xlabel('Frequency  (Hz)');
ylabel('EVM (%)');
legend('EVM - vRef vs. vRx');
figure;
bar(fc(1:end-1),movmean(evm(1:end-1)*100,3),'k');
hold on
xlabel('Frequency  (Hz)');
ylabel('EVM (%)');
% legend('EVM - vRef vs. vRx');
% title('EVM');
% legend('teste 1')

%Plots de Teste para multiplos bars
% bar(fc(1:end-1),evm(1:end-1)*100,'k');
% hold on
% bar(fc(1:end-1),evm(1:end-1)*50,'r');
% xlabel('Frequency  (Hz)');
% ylabel('EVM (%)');
% % legend('EVM - vRef vs. vRx');
% title('EVM respecting LED current-to-light cutoff frequency');
% legend('teste 1','teste 2');
% ylim([0 20])
% subplot 212
% plot(fc,delay,'rx');
% xlabel('Frequency  (Hz)');
% ylabel('delay');
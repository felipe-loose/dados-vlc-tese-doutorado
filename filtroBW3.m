function [rcos] = filtroBW3(time,b,BW,fc)
%Desc: Esta funcao cria um filtro cosseno levantado
%Retorna o filtro no dominio do tempo
%Entradas:
% time = vetor do tempo
% b = fator de roll-of
% BW = largura de banda do filtro
% fc = frequencia central (a portadora do filtro).
% N = numero de pontos da FFT
% Saidas:
% rcos = filtro no dominio do tempo
Ts = 1/BW;
N = length(time);
xs = sinc(2*time*(1/Ts)/2).*2.*cos(2*pi*fc*time); 
%transformacao para cosseno levantado
%filtro cosseno levantado
rcos = xs.*cos(pi*time*b/Ts)./(1-4*b^2.*time.^2/(Ts)^2);
%filtro no dominio da frequencia
end
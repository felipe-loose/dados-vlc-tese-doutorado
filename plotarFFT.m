function [] = plotarFFT(xf,Fs)

%Descricao: Esta funcao plota a magnitude da FFT de uma
% transformada e, dB, normalizada. 
% Entradas:
% xf = transformada de Fourier do sinal em questao;
% Fs = frequencia de amostragem utilizada no sinal;
% x1 e x2 = limites de plotagem do eixo x (frequencia)
% y1 e y2 = limites de plotagem do eixo y (magnitude dB)

%Reorganiza o array das magn
L = length(xf);
Px = 20*log10(abs(xf));
Psx = Px(1:L/2+1);
Psx(2:end-1) = 2*Psx(2:end-1);
f = Fs*(0:(L/2))/L;

%Plota a Figura
figure;
semilogx(f,Psx);
xlabel('frequency (Hz)');
ylabel('magnitude (dB)');
end
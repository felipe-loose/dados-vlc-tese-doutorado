clc; close all; clear all;
format shorteng
load dadosSemPE1a4.mat

iLED = iLED - mean(iLED);
vRx = vRx - mean(vRx);
vRef = vRef - mean(vRef);

Ts = time(3) - time(2);
Fs = 1/Ts;
L = length(time);

%% FFTs

% filtro na banda do sinal
BWf = 3e6;
Tsf = 1/BWf;
fc = 2.5e6;

%Filtro sinc
xs = sinc(2*time*(1/Tsf)/2).*2.*cos(2*pi*fc*time); 
%transformacao para cosseno levantado
b = 0.05;  %roll of
%filtro cosseno levantado
rcos = xs.*cos(pi*time*b/Tsf)./(1-4*b^2.*time.^2/(Tsf)^2);
%filtro no dominio da frequencia
xf = fft(rcos)/L;
% normalizacao
xf = xf/max(abs(xf));

%Plot do filtro na banda do sinal;
Px = 20*log10(abs(xf));
Psx = Px(1:L/2+1);
Psx(2:end-1) = 2*Psx(2:end-1);
f = Fs*(0:(L/2))/L;
figure;
semilogx(f,Psx);
title('FFT do filtro');
ylim([-3 1])
xlim([900e3 4.1e6]);

%% inicio do calculo do EVM por banda
%Filtragem do c�lculo do EVM por banda
Nc = 32; %numero de carriers 
fHi = 3.95e6; %Frequencia inferior da banda
fLo = 1.05e6; %Frequencia superior da banda
BW = 1*(fHi-fLo)/Nc; %banda de cada carrier
Ts = (1/BW);
% fc = linspace(fHi+BW,fLo,128);  %vetor de portadoras
% fc = linspace(fHi,fLo,Nc);  %vetor de portadoras
fc = linspace(fLo,fHi,Nc);  %vetor de portadoras
%parametros fixos do filtro cosseno levantado
 b = 0.1;  %roll of

Vif = fft(iLED/rms(iLED))/L; %Sinal vi(t) (entrada) normalizado
ianalise = it;
If = fft(iLED/rms(iLED))/L; %Corrente i(t) (saida) normalizada
evmBanda = zeros(Nc,1); %Inicia vetor do EVM
% Ib = zeros(length(t2),1);
% Vb = zeros(length(t2),1);
% Vit2 = zeros(lenght(t2),1);
% Ift2 = zeros(lenght(t2),1);
% Vit2 = zeros(length(t2),1);
% If2 = zeros(length(t2),1);
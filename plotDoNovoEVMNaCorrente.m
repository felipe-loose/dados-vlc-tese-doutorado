clc; close all; clear all;

%C�digo que obtem o grafico do EVM usado no artigo do hibrido 
% codigo para a corrente vs. sinal referencia

load evmIPE.mat;
load evmILow.mat;
load evmInoPE.mat;

figure;
subplot 311
bar(fcILo,evmILo*100,'k');
legend('evm LO');
subplot 312
bar(fcIPE,evmIPE*100,'k');
legend('evm HI PE');
subplot 313
bar(fcInoPE,evmInoPE*100,'k');
legend('evm HI no PE');


vetorFreq = linspace(200e3,4e6,64);

vetorEVM = [evmILo(1:31)' evmInoPE(1:13)' evmIPE(13:end)']*100;

vetorEVMSPE = movmean([evmILo(1:end)' evmInoPE']*100,1);

figure;
bar(vetorFreq,vetorEVMSPE,'r');
hold on;
bar(vetorFreq,vetorEVM,'k');
% legend('Without PE','With PE');
xlabel('frequency (Hz)');
ylabel('EVM (%)');
title('EVM from reference to LED current signal');
hold on;
aux = ones(length(vetorFreq),1);
plot(vetorFreq,aux*30,'m--')
plot(vetorFreq,aux*25,'m--')
plot(vetorFreq,aux*15,'m--')
plot(vetorFreq,aux*10,'m--')
plot(vetorFreq,aux*5,'m--')
% plot(vetorFreq,1.75e6,'b--');
yl = ylim;
x = 1.75e6;
line([x, x], yl); % Old way of doing xline().
mx = 2.8e6;
% text(mx,32,'Limit for BPSK (M=2)','Color','m');
% text(mx,27,'Limit for QPSK (M=4)','Color','m');
% text(mx,17,'Limit for 8PSK (M=8)','Color','m');
% text(mx,12,'Limit for 16-QAM','Color','m');
% text(mx,7,'Limit for 32-QAM','Color','m')
text(200e3+20e3,vetorEVM(1),'\leftarrow 200 kHz');

Vqam32 = 5;
Vqam16 = 10;
Vpsk8 = 15;
Vqpsk = 25;
Vbpsk = 30;
vmods = [];
qam32 = 1;
qam16 = 2;
psk8 = 3;
qpsk = 4;
bpsk = 5;

vMod = zeros(length(vetorEVM),1);
j = 1;
i = 1;
rate = 0;
BWcarrier = (vetorFreq(end)-vetorFreq(1))/length(vetorFreq);

while j == 1
    
    n = vetorEVM(i);
    
    if n > 30
        vMod(i) = 0;
        i = i+1;  
        n = vetorEVM(i);
    end 
    
    if n < 5
        vMod(i) = qam32;
        i = i+1;
        n = vetorEVM(i);
        rate = rate + BWcarrier*log2(32);
    end
   
    if n < 10
        vMod(i) = qam16;
        i = i+1;
        n = vetorEVM(i);
        rate = rate + BWcarrier*log2(16);
    end
   
    if n < 15
        vMod(i) = psk8;
        i = i+1;
        n = vetorEVM(i);
        rate = rate + BWcarrier*log2(8);
    end
    
    if n < 25
        vMod(i) = qpsk;
        i = i+1;
        n = vetorEVM(i);
        rate = rate + BWcarrier*log2(4);
    end
   
    if n < 30
        vMod(i) = bpsk;
        i = i+1;  
        n = vetorEVM(i);
        rate = rate + BWcarrier*log2(2);
    end 
    
    if i >= length(vetorEVM)
        break
    end
    
end

rate